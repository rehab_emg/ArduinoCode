#include  <SoftwareSerial.h> //軟件串口
#define RxD 2
#define TxD 3

SoftwareSerial BLE(RxD, TxD);

// get analog value
int getAnalog(int pin)
{
  long sum = 0;

  for (int i = 0; i < 32; i++)
  {
    sum += analogRead(pin);
  }

  return sum >> 5;
}

void setup()
{
  Serial.begin(9600);
  pinMode(RxD, INPUT);
  pinMode(TxD, OUTPUT);
  setupBleConnection();

  Serial.print("Init Setup");
}

void loop()
{
  int val = getAnalog(A0); // get Analog value

  Serial.print(val);
  Serial.print("\n");
  BLE.print(val);

  delay(50);
}

void setupBleConnection()
{
  BLE.begin(9600); //Set BLE BaudRate to default baud rate 9600
}
